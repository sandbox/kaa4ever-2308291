== DEVICE CACHE ==

-- Summary --
Device Cache caches pages separate for users on a mobile and other users. This means that cached pages on a desktop will not be displayed on the mobile and vice versa.
Device Cache is intended for developers in those edge cases, where you have to differ between a mobile user, and a regular user.


-- Installation --
Enable the module and go to the performance settings (admin/config/development/performance),
where you will find a fieldset for "Device Cache" with one checkbox.
Note that the device cache will only be truly activated, if page caching is activated as well.


-- Usage --
The module relies on the PHP Mobile Detect library, so it is recommended to use that library in your code as well.
By example, this means that:
   $detect = new Mobile_Detect;
   if ($detect->isMobile()) {
     // Do stuff for mobiles.
   }
will actually be respected by the cache.


-- Author --
Kristian Kaa (kaa4ever) kaakristian@gmail.com
