<?php
/**
 * @file
 * Administration functions for the device cache module.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function device_cache_form_system_performance_settings_alter(&$form, &$form_state, $form_id) {
  $form['fs_device_cache'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Device Cache'),
    '#collapsible' => FALSE,
  );
  $form['fs_device_cache']['device_cache_active'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Activate device cache'),
    '#description'   => t('Mark this to active a separate cache for mobile users'),
    '#default_value' => variable_get('device_cache_active', FALSE),
  );
}